@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex align-text-center">
                            <h3 >Edit Answers</h3>
                            {{--                            <h3 class="ml-auto">--}}
                            {{--                                <a class="btn btn-sm btn-outline-dark" style="border-radius: 12px;" href="{{route('questions.index')}}">Question List</a>--}}
                            {{--                            </h3>--}}
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            {!!   Form::open([
                            'route'=>['questions.answers.update', $question->id, $answer->id],
                            'method'=>'put',
                            ])   !!}
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    {!! Form::textarea('body',$answer->body,[
                                            'class' => 'form-control',
                                            'id' => 'answer_body',
                                            'placeholder' => 'Enter the Answer Description here...',
                                        ]) !!}
                                    @if ($errors->has('body'))
                                        <span style="color: red">*{{ $errors->first('body') }}</span>
                                    @endif
                                </div>
                            </div>
                            {!! Form::button('Answer-Update', [
                                            'class' => 'btn btn-sm btn-primary',
                                            'style'=> 'border-radius: 13px;',
                                            'type' => 'submit',
                                        ]) !!}
                            {!!   Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
