@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex align-text-center">
                            <h3 >{{$question->title}}</h3>
                            <h3 class="ml-auto">
                                <a class="btn btn-sm btn-outline-dark" style="border-radius: 12px;" href="{{route('questions.index')}}">Question List</a>
                            </h3>
                        </div>
                    </div>
                    <div class="media">
                        <div class="d-flex flex-column votes-control ">
                            <a class="vote-down  mt-3 {{Auth::guest() ? 'off':''}}"
                               onclick="event.preventDefault(); document.getElementById('questions-vote-down-{{$question->id}}').submit()" >
                                <i class="fas fa-thumbs-down fa-2x"></i>
                            </a>
                            <form action="/questions/{{$question->id}}/vote" id="questions-vote-down-{{$question->id}}" method="post" style="display: none; ">
                                @csrf
                                <input type="hidden" name="vote" value="-1">
                            </form>
                            <span>
                                {{$question->votes_count}}
                            </span>
                            <a class="vote-up {{Auth::guest() ? 'off':''}}"
                                   onclick="event.preventDefault(); document.getElementById('questions-vote-up-{{$question->id}}').submit()" >
                                <i class="fas fa-thumbs-up fa-2x"></i>
                            </a>
                            <form action="/questions/{{$question->id}}/vote" id="questions-vote-up-{{$question->id}}" method="post" style="display: none; ">
                                @csrf
                                <input type="hidden" name="vote" value="1">
                            </form>



                            <a class="favourite fabs mt-2 {{Auth::guest() ? 'off':($question->is_favourited) ? 'fabs':''}}"
                               onclick="event.preventDefault(); document.getElementById('questions-favourites-{{$question->id}}').submit()">
                                <i class="fas fa-star fa-1x"></i>
                            </a>
                            <span>{{$question->favourites_count}}</span>
                            <form action="/questions/{{$question->id}}/favourites" id="questions-favourites-{{$question->id}}" method="post" style="display: none; ">
                                @csrf
                                @if($question->is_favourited)
                                    @method('delete')
                                @endif
                            </form>
                        </div>
                        <div class="media-body">
                   {!!  $question->body_html!!}
                        <div class="float-right">
                                            <span class="text-muted">
                                                Questioned {!! $question->create_date !!}
                                            </span>
                            <div class="media">
                                <a class="pr-2" href="{!! $question->user->url !!}">
                                    <img src="{!! $question->user->avatar !!}" alt="avatar">
                                </a>
                                <div class="media-body">
                                    <a href="{!! $question->user->url !!}">
                                        {!! $question->user->name !!}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>

{{--      >>>>>>>>>>>>>>>>>>  This is the Answer Section<<<<<<<<<<<<<<<<<--}}

        <div class="row mt-5">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            @include('layouts._message')
                            <h2>Your Answer</h2>
                            {!!   Form::open([
                            'route'=>['questions.answers.store', $question->id],
                            'method'=>'post',
                            ])   !!}
                            <div class="form-group">
                                {!! Form::textarea('body', null, [
                                        'class' => 'form-control',
                                        'placeholder' => 'Enter Your Answer Description here...',
                                    ]) !!}
                                @if ($errors->has('body'))
                                    <span style="color: red">*{{ $errors->first('body') }}</span>
                                @endif

                            </div>
                            {!! Form::button('Submit-Answer', [
                                            'class' => 'btn btn-sm btn-primary',
                                            'style'=> 'border-radius: 13px;',
                                            'type' => 'submit',
                                        ]) !!}
                            {!!   Form::close() !!}
                            <hr>
                            <h4>
                                {!! $question->answers_count !!}
                                {!! str_plural('- Answer' , $question->answers_count) !!}
                            </h4>
                            <hr>
                            @foreach($question->answers as $answer)
                                <div class="media">
                                    <div class="d-flex flex-column votes-control ">
                                        <a class="vote-down  mt-3 {{Auth::guest() ? 'off':''}}"
                                           onclick="event.preventDefault(); document.getElementById('answers-vote-down-{{$answer->id}}').submit()" >
                                            <i class="fas fa-thumbs-down fa-2x"></i>
                                        </a>
                                        <form action="/answers/{{$answer->id}}/vote" id="answers-vote-down-{{$answer->id}}" method="post" style="display: none; ">
                                            @csrf
                                            <input type="hidden" name="vote" value="-1">
                                        </form>
                                        <span>
                                            {{$answer->votes_count}}
                                        </span>
                                        <a class="vote-up {{Auth::guest() ? 'off':''}}"
                                           onclick="event.preventDefault(); document.getElementById('answers-vote-up-{{$answer->id}}').submit()" >
                                            <i class="fas fa-thumbs-up fa-2x"></i>
                                        </a>
                                        <form action="/answers/{{$answer->id}}/vote" id="answers-vote-up-{{$answer->id}}" method="post" style="display: none; ">
                                            @csrf
                                            <input type="hidden" name="vote" value="1">
                                        </form>
                                        <a class="favourite fabs mt-2 {{Auth::guest() ? 'off':($question->is_favourited) ? 'fabs':''}}"
                                           onclick="event.preventDefault(); document.getElementById('questions-favourites-{{$question->id}}').submit()">
                                            <i class="fas fa-star fa-1x"></i>
                                        </a>
                                        <span>{{$question->favourites_count}}</span>
                                        <form action="/questions/{{$question->id}}/favourites" id="questions-favourites-{{$question->id}}" method="post" style="display: none; ">
                                            @csrf
                                            @if($question->is_favourited)
                                                @method('delete')
                                            @endif
                                        </form>
                                        @can('accept', $answer)
                                        <a class="favourite mt-4 {{$answer->status}}" onclick="event.preventDefault(); document.getElementById('accept-answer-{{$answer->id}}').submit()" >
                                            <i class="fas fa-check fa-2x"></i>
                                        </a>
                                            <form action="{{route('accept.answers', $answer->id)}}" method="post" style="display:none;" id="accept-answer-{{$answer->id}}">
                                                @csrf
                                            </form>
                                            @else
                                                @if($answer->is_best)
                                                    <a class="favourite mt-4 {{$answer->status}}" >
                                                        <i class="fas fa-check fa-2x"></i>
                                                    </a>
                                                @endif
                                        @endcan
                                    </div>
                                    <div class="media-body">
                                        {!! $answer->body_html !!}
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="ml-auto">
                                                    @can('update', $answer)
                                                        <a class="btn btn-sm btn-outline-success" style="border-radius:10px" href="{{route('questions.answers.edit', [$question->id, $answer->id])}}">Edit</a>
                                                    @endcan
                                                    @can('delete', $answer)
                                                        {!! Form::open([
                                                        'route'=>['questions.answers.destroy', $question->id,$answer->id],
                                                        'method'=> 'delete',
                                                        'style'=> 'display:inline'
                                                    ]) !!}
                                                        {!! Form::button('Delete', [
                                                            'type'=> 'submit',
                                                            'class'=>'btn btn-outline-danger btn-sm',
                                                            'style'=>'border-radius:12px',
                                                            'onclick'=> 'return confirm("Are You Sure , Want To Delete It Permanently?" )',
                                                        ]) !!}
                                                        {!! Form::close() !!}
                                                    @endcan
                                                </div>
                                            </div>
                                            <div class="col-md-4"></div>
                                            <div class="col-md-4">
                                                <div class="float-right">
                                                    <span class="text-muted">
                                                        Answered {!! $answer->create_date !!}
                                                    </span>
                                                    <div class="media">
                                                        <div class="media-body">
                                                            <a class="pr-2" href="{!! $answer->user->url !!}">
                                                                <img src="{!! $answer->user->avatar !!}" alt="avatar">
                                                            </a>

                                                            <a class="pr-2" href="{!! $answer->user->url !!}">
                                                                {!! $answer->user->name !!}
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
