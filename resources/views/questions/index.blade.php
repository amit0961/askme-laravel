@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex align-text-center">
                            <h3 >All Questions</h3>
                            <h3 class="ml-auto">
                                <a class="btn btn-sm btn-outline-dark" style="border-radius: 12px;" href="{{route('questions.create')}}">Ask me</a>
                            </h3>
                        </div>
                    </div>
                    @include('layouts._message')
                    @foreach($questions as $question)
                        <div class="card-body">
                            <div class="media">
                                <div class="d-flex flex-column counters">
                                    <div class="vote">
                                        <strong style="display: block; font-size: 2em;"> {{$question->votes_count}}</strong>
                                        {{str_plural('vote', $question->votes_count)}}
                                    </div>
                                    <div class="status {{$question->status}}">
                                        <strong style="display: block; font-size: 2em;">{{$question->answers_count}}</strong>
                                        {{str_plural('answer', $question->answers_count)}}
                                    </div>
                                    <div class="views" style="margin-top: 10px;">
                                      {{$question->views}}
                                        {{str_plural('view', $question->views)}}
                                    </div>
                                </div>
                                <div class="media-body">
                                    <div class="d-flex align-items-center">
                                        <h3 class="mt-3">
                                            <a href="{{$question->url}}">
                                                {{$question->title }}
                                            </a>
                                        </h3>
                                        <div class="ml-auto">
                                            @can('update', $question)
                                            <a class="btn btn-sm btn-outline-success" style="border-radius:10px" href="{{route('questions.edit', $question->id)}}">Edit</a>
                                            @endcan
                                            @can('delete', $question)
                                                {!! Form::open([
                                                'route'=>['questions.destroy', $question->id],
                                                'method'=> 'delete',
                                                'style'=> 'display:inline'
                                            ]) !!}
                                            {!! Form::button('Delete', [
                                                'type'=> 'submit',
                                                'class'=>'btn btn-outline-danger btn-sm',
                                                'style'=>'border-radius:12px',
                                                'onclick'=> 'return confirm("Are You Sure , Want To Delete It Permanently?" )',
                                            ]) !!}
                                            {!! Form::close() !!}
                                                @endcan
                                        </div>
                                    </div>
                                    <p class="lead">
                                        Asked By:-
                                        <a href="#">
                                            {{$question->user->name}}
                                        </a>
                                        <small class="text-muted">
                                            {{$question->create_date}}
                                        </small>
                                    </p>
                                    {{str_limit($question->body, 250)}}
                                </div>
                            </div>
                        </div>
                        <hr>
                    @endforeach
                    <ul class="pagination justify-content-center mb-4">
                        <li class="page-item">
                            {{$questions->links()}}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
