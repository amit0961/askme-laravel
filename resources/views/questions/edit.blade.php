@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex align-text-center">
                            <h3 >Edit Questions</h3>
{{--                            <h3 class="ml-auto">--}}
{{--                                <a class="btn btn-sm btn-outline-dark" style="border-radius: 12px;" href="{{route('questions.index')}}">Question List</a>--}}
{{--                            </h3>--}}
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            {!!   Form::open([
                            'route'=>['questions.update', $question->id],
                            'method'=>'put',
                            ])   !!}
                            <div class="form-group row">
                                {!! Form::label('Question-Title :-',$question->title, ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::text( 'title', $question->title, [
                                        'class'=>'form-control',
                                        'id'=>'question_title',
                                        'placeholder' => 'Enter the Questions Title here...',

                                     ]) !!}
                                    @if ($errors->has('title'))
                                        <span style="color: red">*{{ $errors->first('title') }}</span>
                                    @endif

                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('Question-Body:-', $question->body, ['class' => 'col-sm-3 col-form-label']) !!}

                                <div class="col-sm-10">
                                    {!! Form::textarea('body', $question->body, [
                                            'class' => 'form-control',
                                            'id' => 'question_body',
                                            'placeholder' => 'Enter the Questions Description here...',

                                        ]) !!}
                                    @if ($errors->has('body'))
                                        <span style="color: red">*{{ $errors->first('body') }}</span>
                                    @endif

                                </div>
                            </div>
                            {!! Form::button('Question-Update', [
                                            'class' => 'btn btn-sm btn-primary',
                                            'style'=> 'border-radius: 13px;',
                                            'type' => 'submit',
                                        ]) !!}

                            {!!   Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
